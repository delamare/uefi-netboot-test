#!/bin/sh

set -x
set -m


# Setup virtual network
sudo ip tuntap add dev virttap mode tap
sudo brctl addbr virtbr
sudo brctl addif virtbr virttap
sudo ip link set virtbr up
sudo ip link set virttap up

sudo ip a add 10.0.0.1/24 dev virtbr



# Set dnsmasq for DHCP/PXE/TFTP
cat > dnsmasq.conf << EOF
interface=virtbr

dhcp-range=10.0.0.151,10.0.0.200,12h
dhcp-option=option:router,10.0.0.1

dhcp-boot=grub/x86_64-efi/core.efi

# TFTP Server setup
enable-tftp
tftp-root=$(pwd)
EOF


# Install Grub as network bootloader
grub-mknetdir --net-directory=. --subdir=grub
mkdir -p grub
cat > grub/grub.cfg << "EOF"
insmod http
source (http,10.0.0.1:8000)/grub/bootselector.cfg

set default=${bootselector}
set timeout=5

menuentry 'sys1' {
  insmod part_gpt
  chainloader (hd0,gpt1)/EFI/sys1/grubx64.efi
}
menuentry 'sys2' {
  insmod part_gpt
  chainloader (hd0,gpt1)/EFI/sys2/grubx64.efi
}
menuentry 'deployenv' {
  insmod tftp
  linux (tftp)/kadeploy3-deploy-kernel-jessie.vmlinuz initrd=kadeploy3-deploy-kernel-jessie.initrd.img ramdisk_size=260000 rw noapic init=/linuxrc
  initrd (tftp)/kadeploy3-deploy-kernel-jessie.initrd.img
}
EOF
cat > grub/bootselector.cfg << EOF
set bootselector=sys2
EOF


# Start VM, dnsmasq, HTTP server
## From QEMU, press ESC to select network boot
sleep 1
(sudo -E qemu-system-x86_64 -boot order=nc -m 2048 -k fr --bios /usr/share/OVMF/OVMF_CODE.ms.fd -net nic -net tap,ifname=virttap,script=no,downscript=no --hda test-disk.img & \
sudo dnsmasq -C dnsmasq.conf -d & \
python3 -m http.server)


# Clean
sudo ip l delete virttap
sudo ip l delete virtbr
