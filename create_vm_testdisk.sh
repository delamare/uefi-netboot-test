#!/bin/bash
set -x
set -e

truncate -s 4g test-disk.img
/sbin/parted test-disk.img -a minimal -s mklabel gpt
/sbin/parted test-disk.img -a minimal -s mkpart efi fat32 1M 512M
/sbin/parted test-disk.img -a minimal -s mkpart primary ext4 512M 2048M
/sbin/parted test-disk.img -a minimal -s mkpart primary ext4 2048M 100%
/sbin/parted test-disk.img -a minimal -s toggle 1 boot

LODEV=$(sudo losetup  --partscan --show --find  test-disk.img)
sudo mkdosfs -F 32 ${LODEV}p1
sudo mkfs.ext4 ${LODEV}p2
sudo mkfs.ext4 ${LODEV}p3

mkdir -p mnt/{sys1,sys2,efi}
sudo mount ${LODEV}p1 mnt/efi
sudo mount ${LODEV}p2 mnt/sys1
sudo mount ${LODEV}p3 mnt/sys2
sudo debootstrap --include linux-image-amd64 stretch mnt/sys1
sudo debootstrap --include linux-image-amd64 stretch mnt/sys2

sudo grub-install --target=x86_64-efi --efi-directory=mnt/efi/ --bootloader-id=sys1 --boot-directory=mnt/sys1/boot/
sudo grub-install --target=x86_64-efi --efi-directory=mnt/efi/ --bootloader-id=sys2 --boot-directory=mnt/sys2/boot/
sudo grub-mkimage -o mnt/efi/EFI/sys1/grubx64.efi --format=x86_64-efi '--prefix=(hd0,gpt2)/boot/grub' ext2 part_gpt
sudo grub-mkimage -o mnt/efi/EFI/sys2/grubx64.efi --format=x86_64-efi '--prefix=(hd0,gpt3)/boot/grub' ext2 part_gpt

sudo su -c "cat > mnt/sys1/boot/grub/grub.cfg << EOF
set default=1
set timeout=0

menuentry 'linux' {
  set root=(hd0,2)
  linux /vmlinuz root=/dev/sda2 ro
  initrd /initrd.img
}
EOF
"
sudo su -c "cat > mnt/sys2/boot/grub/grub.cfg << EOF
set default=1
set timeout=0

menuentry 'linux' {
  set root=(hd0,3)
  linux /vmlinuz root=/dev/sda3 ro
  initrd /initrd.img
}
EOF
"
echo sys1 | sudo tee mnt/sys1/etc/hostname
echo sys2 | sudo tee mnt/sys2/etc/hostname

sudo umount mnt/*
sudo losetup -d $LODEV
rmdir --parents mnt/*
